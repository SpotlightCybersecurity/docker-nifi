FROM apache/nifi:1.13.2
USER root
RUN apt-get update && apt-get install -y python3 python3-pip && pip3 install requests pytz && rm -rf /var/lib/{apt,dpkg,cache,log}
RUN mkdir /conf /conf/archive /data /data/flowfile_repository /data/content_repository /data/provenance_repository /data/database_repository /data/state && chown -R nifi /conf /data
COPY sc_start.sh /opt/nifi/scripts/
USER nifi
ENTRYPOINT ["../scripts/sc_start.sh"]
