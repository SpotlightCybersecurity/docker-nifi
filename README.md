An [Apache NiFi](https://nifi.apache.org) container that is more friendly for standalone use. And it includes Python for running Python scripts via the `ExecuteStreamCommand` processor.

Run this container like this (substituting the directories and ports for your setup).
```
docker run --rm -d --name nifi -e NIFI_SENSITIVE_PROPS_KEY=YourSensitivePropertiesKeyGoesHere -v /path/to/database:/data -v /path/to/config:/conf -p 8080:8080 spotlightcybersecurity/nifi
```

The container will put the persistent databases which track flowfiles in `/data` and the persistent flow configuration files in `/conf` (the NiFi properties are set via environment variables). Make sure both volumes are writeable by the `nifi` user inside the container.

# Notes About Security of NiFi in a container
This container listens on port 8000 with no authentication by default. Protect access to it by some other means: enable NiFi's built-in authentication or put a proxy in front of it (e.g. nginx).

# More info about Nifi
* [Apache NiFi Official Site](https://nifi.apache.org)
* [Apache Nifi Documentation](https://nifi.apache.org/docs.html) - List of Processors, User Guide (how to use the UI and setup flows), Admin Guide (how to setup authentication/authorization)