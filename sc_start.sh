#!/bin/sh -e
. /opt/nifi/scripts/common.sh
prop_replace nifi.flow.configuration.file /conf/flow.xml.gz
prop_replace nifi.flow.configuration.archive.dir /conf/archive
prop_replace nifi.flowfile.repository.directory /data/flowfile_repository
prop_replace nifi.database.directory /data/database_repository
prop_replace nifi.provenance.repository.directory.default /data/provenance_repository
prop_replace nifi.content.repository.directory.default /data/content_repository
prop_replace nifi.sensitive.props.key "${NIFI_SENSITIVE_PROPS_KEY:-}"
sed -i -e 's|\./state/local|/data/state|' /opt/nifi/nifi-current/conf/state-management.xml
for dir in /conf/archive /data/database_repository /data/provenance_repository /data/flowfile_repository /data/state /data/content_repository; do
    if [ ! -e "$dir" ]; then echo "$dir doesn't exist, creating!"; mkdir "$dir"; fi
done
echo "resuming normal nifi startup"
exec ../scripts/start.sh